package test;

import com.rohit.ipl.Delivery;
import com.rohit.ipl.Main;
import com.rohit.ipl.Match;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class MainTest {
    private static List<Match> matches;
    private static List<Delivery> deliveries;

    @BeforeAll
    public static void beforeAll() {
        matches = Main.getMatchesData();
        deliveries = Main.getDeliveriesData();
    }

    @Test
    public void testGetMatchesData() {
        List<Match> actualDataOfMatches = Main.getMatchesData();
        assertEquals(636, actualDataOfMatches.size());
        assertNotNull(actualDataOfMatches);
    }

    @Test
    public void testGetDeliveriesData() {
        List<Delivery> actualDataOfDeliveries = Main.getDeliveriesData();
        assertEquals(150460, actualDataOfDeliveries.size());
        assertNotNull(actualDataOfDeliveries);
    }

    @Test
    public void testGetNumberOfMatchesPlayedPerYear() {
        Map<String, Integer> actualDataOfMatchesPlayedPerYear = Main.getNumberOfMatchesPlayedPerYear(matches);
        Map<String, Integer> expectedDataOfMatchesPlayedPerYear = new HashMap<>();
        expectedDataOfMatchesPlayedPerYear.put("2008", 58);
        expectedDataOfMatchesPlayedPerYear.put("2009", 57);
        expectedDataOfMatchesPlayedPerYear.put("2010", 60);
        expectedDataOfMatchesPlayedPerYear.put("2011", 73);
        expectedDataOfMatchesPlayedPerYear.put("2012", 74);
        expectedDataOfMatchesPlayedPerYear.put("2013", 76);
        expectedDataOfMatchesPlayedPerYear.put("2014", 60);
        expectedDataOfMatchesPlayedPerYear.put("2015", 59);
        expectedDataOfMatchesPlayedPerYear.put("2016", 60);
        expectedDataOfMatchesPlayedPerYear.put("2017", 59);

        assertNotNull(actualDataOfMatchesPlayedPerYear);

        for (Map.Entry<String, Integer> entry : actualDataOfMatchesPlayedPerYear.entrySet()) {
            assertEquals(expectedDataOfMatchesPlayedPerYear.get(entry.getKey()),
                    actualDataOfMatchesPlayedPerYear.get(entry.getKey()));
        }
    }

    @Test
    public void testGetNumberOfMatchesWonAllTeamInAllSeason() {
        Map<String, Integer> actualDataOfTeamsWonMatchesInIpl = Main.getNumberOfMatchesWonAllTeamInAllSeason(matches);
        Map<String, Integer> expectedDataOfTeamsWonMatchesInIpl = new HashMap<>();
        expectedDataOfTeamsWonMatchesInIpl.put("Mumbai Indians", 92);
        expectedDataOfTeamsWonMatchesInIpl.put("Sunrisers Hyderabad", 42);
        expectedDataOfTeamsWonMatchesInIpl.put("Pune Warriors", 12);
        expectedDataOfTeamsWonMatchesInIpl.put("Rajasthan Royals", 63);
        expectedDataOfTeamsWonMatchesInIpl.put("Kolkata Knight Riders", 77);
        expectedDataOfTeamsWonMatchesInIpl.put("Royal Challengers Bangalore", 73);
        expectedDataOfTeamsWonMatchesInIpl.put("Gujarat Lions", 13);
        expectedDataOfTeamsWonMatchesInIpl.put("Rising Pune Supergiant", 10);
        expectedDataOfTeamsWonMatchesInIpl.put("Kochi Tuskers Kerala", 6);
        expectedDataOfTeamsWonMatchesInIpl.put("Kings XI Punjab", 70);
        expectedDataOfTeamsWonMatchesInIpl.put("Deccan Chargers", 29);
        expectedDataOfTeamsWonMatchesInIpl.put("Delhi Daredevils", 62);
        expectedDataOfTeamsWonMatchesInIpl.put("Rising Pune Supergiants", 5);
        expectedDataOfTeamsWonMatchesInIpl.put("Chennai Super Kings", 79);

        assertNotNull(actualDataOfTeamsWonMatchesInIpl);

        for (Map.Entry<String, Integer> entry : actualDataOfTeamsWonMatchesInIpl.entrySet()) {
            assertEquals(expectedDataOfTeamsWonMatchesInIpl.get(entry.getKey()),
                    actualDataOfTeamsWonMatchesInIpl.get(entry.getKey()));
        }
    }

    @Test
    public void testGetExtraRunsPerTeamIn2016() {
        Map<String, Integer> actualDataOfGetExtraRunsByTeam = Main.getExtraRunsPerTeamIn2016(matches, deliveries);
        Map<String, Integer> expectedDataOfGetExtraRunsByTeam = new HashMap<>();

        expectedDataOfGetExtraRunsByTeam.put("Gujarat Lions", 132);
        expectedDataOfGetExtraRunsByTeam.put("Mumbai Indians", 102);
        expectedDataOfGetExtraRunsByTeam.put("Sunrisers Hyderabad", 124);
        expectedDataOfGetExtraRunsByTeam.put("Kings XI Punjab", 83);
        expectedDataOfGetExtraRunsByTeam.put("Delhi Daredevils", 109);
        expectedDataOfGetExtraRunsByTeam.put("Rising Pune Supergiants", 101);
        expectedDataOfGetExtraRunsByTeam.put("Kolkata Knight Riders", 130);
        expectedDataOfGetExtraRunsByTeam.put("Royal Challengers Bangalore", 118);

        assertNotNull(actualDataOfGetExtraRunsByTeam);

        for (Map.Entry<String, Integer> entry : actualDataOfGetExtraRunsByTeam.entrySet()) {
            assertEquals(expectedDataOfGetExtraRunsByTeam.get(entry.getKey()),
                    actualDataOfGetExtraRunsByTeam.get(entry.getKey()));
        }
    }

    @Test
    public void testGetTopTenEconomicalBowlersForYear2015() {
        Map<String, Double> actualDataOfTopTenBowlersWithEconomyRate
                = Main.getTopTenEconomicalBowlersForYear2015(matches, deliveries);
        Map<String, Double> expectedDataOfTopTenBowlersWithEconomyRate = new HashMap<>();

        expectedDataOfTopTenBowlersWithEconomyRate.put("RN ten Doeschate", 3.4285714285714284);
        expectedDataOfTopTenBowlersWithEconomyRate.put("J Yadav", 4.142857142857143);
        expectedDataOfTopTenBowlersWithEconomyRate.put("V Kohli", 5.454545454545455);
        expectedDataOfTopTenBowlersWithEconomyRate.put("R Ashwin", 5.725);
        expectedDataOfTopTenBowlersWithEconomyRate.put("S Nadeem", 5.863636363636364);
        expectedDataOfTopTenBowlersWithEconomyRate.put("Z Khan", 6.15483870967742);
        expectedDataOfTopTenBowlersWithEconomyRate.put("Parvez Rasool", 6.2);
        expectedDataOfTopTenBowlersWithEconomyRate.put("MC Henriques", 6.2675159235668785);
        expectedDataOfTopTenBowlersWithEconomyRate.put("MA Starc", 6.75);
        expectedDataOfTopTenBowlersWithEconomyRate.put("M de Lange", 6.923076923076923);

        assertNotNull(actualDataOfTopTenBowlersWithEconomyRate);

        for (Map.Entry<String, Double> entry : actualDataOfTopTenBowlersWithEconomyRate.entrySet()) {
            assertEquals(expectedDataOfTopTenBowlersWithEconomyRate.get(entry.getKey()),
                    actualDataOfTopTenBowlersWithEconomyRate.get(entry.getKey()));
        }
    }

    @Test
    public void testGetNumberOfSeasonsInIpl() {
        int actualTotalSeason = Main.getNumberOfSeasonsInIpl(matches);
        assertEquals(10, actualTotalSeason);
    }

    @AfterAll
    static void afterAll() {
        matches = null;
        deliveries = null;
    }
}