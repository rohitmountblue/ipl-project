package com.rohit.ipl;

import java.util.*;
import java.io.*;

public class Main {
    public static final int ID = 0;
    public static final int SEASON = 1;
    public static final int CITY = 2;
    public static final int DATE = 3;
    public static final int TEAM1 = 4;
    public static final int TEAM2 = 5;
    public static final int TOSS_WINNER = 6;
    public static final int TOSS_DECISION = 7;
    public static final int RESULT = 8;
    public static final int DL_APPLIED = 9;
    public static final int WINNER = 10;
    public static final int WIN_BY_RUNS = 11;
    public static final int WIN_BY_WICKETS = 12;
    public static final int PLAYER_OF_MATCH = 13;
    public static final int VENUE = 14;
    public static final int UMPIRE1 = 15;
    public static final int UMPIRE2 = 16;
    public static final int UMPIRE3 = 17;
    public static final int MATCH_ID = 0;
    public static final int INNING = 1;
    public static final int BATTING_TEAM = 2;
    public static final int BOWLING_TEAM = 3;
    public static final int OVER = 4;
    public static final int BALL = 5;
    public static final int BATSMAN = 6;
    public static final int NON_STRIKER = 7;
    public static final int BOWLER = 8;
    public static final int IS_SUPER_OVER = 9;
    public static final int WIDE_RUNS = 10;
    public static final int BYE_RUNS = 11;
    public static final int LEG_BYE_RUNS = 12;
    public static final int NO_BALL_RUNS = 13;
    public static final int PENALITY_RUNS = 14;
    public static final int BATSMAN_RUNS = 15;
    public static final int EXTRA_RUNS = 16;
    public static final int TOTAL_RUNS = 17;
    public static final int PLAYER_DISMISSED = 18;
    public static final int DISMISSAL_KIND = 19;
    public static final int FIELDER = 20;

    public static void main(String[] args) {
        List<Match> matches = getMatchesData();
        List<Delivery> deliveries = getDeliveriesData();

        Map<String, Integer> matchesPlayedPerYear = getNumberOfMatchesPlayedPerYear(matches);
        System.out.println(matchesPlayedPerYear);
        Map<String, Integer> teamsWonMatchesInIpl = getNumberOfMatchesWonAllTeamInAllSeason(matches);
        System.out.println(teamsWonMatchesInIpl);
        Map<String, Integer> getExtraRunsByTeam = getExtraRunsPerTeamIn2016(matches, deliveries);
        System.out.println(getExtraRunsByTeam);
        Map<String, Double> topTenBowlersWithEconomy = getTopTenEconomicalBowlersForYear2015(matches, deliveries);
        System.out.println(topTenBowlersWithEconomy);
        int totalSeasons = getNumberOfSeasonsInIpl(matches);
        System.out.println("Total Season: "+totalSeasons);
    }

    public static List<Delivery> getDeliveriesData() {
        List<Delivery> deliveries = new ArrayList<>();
        try {
            BufferedReader reader = new BufferedReader(new FileReader("datasets/deliveries.csv"));
            String line;
            reader.readLine();

            while ((line = reader.readLine()) != null) {
                String[] deliveryRow = line.split(",", -1);
                Delivery delivery = new Delivery();

                delivery.setMatchId(Integer.parseInt(deliveryRow[MATCH_ID]));
                delivery.setInning(Integer.parseInt(deliveryRow[INNING]));
                delivery.setBattingTeam(deliveryRow[BATTING_TEAM]);
                delivery.setBowlingTeam(deliveryRow[BOWLING_TEAM]);
                delivery.setOver(Integer.parseInt(deliveryRow[OVER]));
                delivery.setBall(Integer.parseInt(deliveryRow[BALL]));
                delivery.setBatsman(deliveryRow[BATSMAN]);
                delivery.setNonStriker(deliveryRow[NON_STRIKER]);
                delivery.setBowler(deliveryRow[BOWLER]);
                delivery.setIsSuperOver(Integer.parseInt(deliveryRow[IS_SUPER_OVER]));
                delivery.setWideRuns(Integer.parseInt(deliveryRow[WIDE_RUNS]));
                delivery.setByeRuns(Integer.parseInt(deliveryRow[BYE_RUNS]));
                delivery.setLegByeRuns(Integer.parseInt(deliveryRow[LEG_BYE_RUNS]));
                delivery.setNoBallRuns(Integer.parseInt(deliveryRow[NO_BALL_RUNS]));
                delivery.setPenaltyRuns(Integer.parseInt(deliveryRow[PENALITY_RUNS]));
                delivery.setBatsmanRuns(Integer.parseInt(deliveryRow[BATSMAN_RUNS]));
                delivery.setExtraRuns(Integer.parseInt(deliveryRow[EXTRA_RUNS]));
                delivery.setTotalRuns(Integer.parseInt(deliveryRow[TOTAL_RUNS]));
                delivery.setPlayerDismissed(deliveryRow[PLAYER_DISMISSED]);
                delivery.setDismissalKind(deliveryRow[DISMISSAL_KIND]);
                delivery.setFielder(deliveryRow[FIELDER]);

                deliveries.add(delivery);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return deliveries;
    }

    public static List<Match> getMatchesData() {
        List<Match> matches = new ArrayList<>();
        try {
            BufferedReader reader = new BufferedReader(new FileReader("datasets/matches.csv"));
            String line;
            reader.readLine();

            while ((line = reader.readLine()) != null) {
                String[] matchRow = line.split(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)", -1);

                Match match = new Match();
                match.setId(Integer.parseInt(matchRow[ID]));
                match.setSeason(matchRow[SEASON]);
                match.setCity(matchRow[CITY]);
                match.setDate(matchRow[DATE]);
                match.setTeam1(matchRow[TEAM1]);
                match.setTeam2(matchRow[TEAM2]);
                match.setTossWinner(matchRow[TOSS_WINNER]);
                match.setTossDecision(matchRow[TOSS_DECISION]);
                match.setResult(matchRow[RESULT]);
                match.setDlApplied(Integer.parseInt(matchRow[DL_APPLIED]));
                match.setWinner(matchRow[WINNER]);
                match.setWinByRuns(Integer.parseInt(matchRow[WIN_BY_RUNS]));
                match.setWinByWickets(Integer.parseInt(matchRow[WIN_BY_WICKETS]));
                match.setPlayerOfMatch(matchRow[PLAYER_OF_MATCH]);
                match.setVenue(matchRow[VENUE]);
                match.setUmpire1(matchRow[UMPIRE1]);
                match.setUmpire2(matchRow[UMPIRE2]);
                match.setUmpire3(matchRow[UMPIRE3]);

                matches.add(match);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return matches;
    }

    public static Map<String, Integer> getNumberOfMatchesPlayedPerYear(List<Match> matches) {
        Map<String, Integer> matchesPlayedPerYear = new TreeMap<>();

        for (Match match : matches) {
            if (!matchesPlayedPerYear.containsKey(match.getSeason())) {
                matchesPlayedPerYear.put(match.getSeason(), 1);
            } else {
                matchesPlayedPerYear.put(match.getSeason(), matchesPlayedPerYear.getOrDefault(
                        match.getSeason(), 0) + 1);
            }
        }
        return matchesPlayedPerYear;
    }

    public static Map<String, Integer> getNumberOfMatchesWonAllTeamInAllSeason(List<Match> matches) {
        Map<String, Integer> teamsWonMatchesInIpl = new HashMap<>();

        for (Match match : matches) {
            if (!match.getWinner().equals("")) {
                if (!teamsWonMatchesInIpl.containsKey(match.getWinner())) {
                    teamsWonMatchesInIpl.put(match.getWinner(), 1);
                } else {
                    teamsWonMatchesInIpl.put(match.getWinner(),
                            teamsWonMatchesInIpl.getOrDefault(match.getWinner(), 0) + 1);
                }
            }
        }
        return teamsWonMatchesInIpl;
    }

    public static Map<String, Integer> getExtraRunsPerTeamIn2016(List<Match> matches, List<Delivery> deliveries) {
        Map<String, Integer> getExtraRunsByTeam = new HashMap<>();
        Set<Integer> matchIds = new HashSet<>();

        for (Match match : matches) {
            if (match.getSeason().equals("2016")) {
                matchIds.add(match.getId());
            }
        }

        for (Delivery delivery : deliveries) {
            if (matchIds.contains(delivery.getMatchId())) {
                getExtraRunsByTeam.put(delivery.getBattingTeam(),
                        getExtraRunsByTeam.getOrDefault(delivery.getBattingTeam(), 0)
                                + delivery.getExtraRuns());
            }
        }
        return getExtraRunsByTeam;
    }

    public static Map<String, Double> getTopTenEconomicalBowlersForYear2015(List<Match> matches, List<Delivery> deliveries) {
        Map<String, Double> bowlersWithEconomy = new HashMap<>();
        Map<String, Bowler> bowlersWithRunAndBalls = new HashMap<>();
        Set<Integer> matchIds = new HashSet<>();

        for (Match match : matches) {
            if (match.getSeason().equals("2015")) {
                matchIds.add(match.getId());
            }
        }

        for (Delivery delivery : deliveries) {
            if (matchIds.contains(delivery.getMatchId())) {
                if (!bowlersWithRunAndBalls.containsKey(delivery.getBowler())) {
                    bowlersWithRunAndBalls.put(delivery.getBowler(), new Bowler(delivery.getTotalRuns(), 1));
                } else {
                    Bowler bowler = bowlersWithRunAndBalls.get(delivery.getBowler());
                    bowlersWithRunAndBalls.put(delivery.getBowler(),
                            new Bowler(bowler.getRun() + delivery.getTotalRuns(), bowler.getBall() + 1));
                }
            }
        }

        for (Map.Entry<String, Bowler> entry : bowlersWithRunAndBalls.entrySet()) {
            double over = (double) entry.getValue().getBall() / 6d;
            double economy = entry.getValue().getRun() / over;
            bowlersWithEconomy.put(entry.getKey(), economy);
        }

        List<Map.Entry<String, Double>> sortBowlersWithEconomy =
                new ArrayList<>(bowlersWithEconomy.entrySet());

        Collections.sort(sortBowlersWithEconomy, new Comparator<Map.Entry<String, Double>>() {
            @Override
            public int compare(Map.Entry<String, Double> bowler1, Map.Entry<String, Double> bowler2) {
                return (bowler1.getValue().compareTo(bowler2.getValue()));
            }
        });

        Map<String, Double> topTenBowlersWithEconomy = new LinkedHashMap<>();
        int countBowler = 0;
        for (Map.Entry<String, Double> entry : sortBowlersWithEconomy) {
            if (countBowler < 10) {
                topTenBowlersWithEconomy.put(entry.getKey(), entry.getValue());
                ++countBowler;
            } else {
                break;
            }
        }
        return topTenBowlersWithEconomy;
    }

    public static int getNumberOfSeasonsInIpl(List<Match> matches) {
        Set<String> seasons = new TreeSet<>();
        int seasonCount = 0;
        for (Match match : matches) {
            if (!seasons.contains(match.getSeason())) {
                seasons.add(match.getSeason());
                seasonCount += 1;
            }
        }
        return seasonCount;
    }
}
