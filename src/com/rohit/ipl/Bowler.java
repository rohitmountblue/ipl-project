package com.rohit.ipl;

public class Bowler {
    private long run;
    private long ball;

    Bowler(long run, long bowl) {
        this.run = run;
        this.ball = bowl;
    }

    public long getRun() {
        return run;
    }

    public void setRun(long run) {
        this.run = run;
    }

    public long getBall() {
        return ball;
    }

    public void setBall(long ball) {
        this.ball = ball;
    }
}
